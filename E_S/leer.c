 #include <sys/types.h>
       #include <sys/stat.h>
       #include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>


int main(){
	umask(0);
	int fd = open("imagen.png", O_RDONLY);

	if(fd < 0){
		perror("error al abrir el archivo\n");
	}

	unsigned char buf[100] = {0};
	int escritos = read(fd, buf, 100);



	if(escritos < 0){
		perror("Error al escribir archivo.");
	}

	for(int i =0 ; i < 100; i++){
		printf("0x%02x ", buf[i]);
	}
	close(fd);


	return 0;
}
