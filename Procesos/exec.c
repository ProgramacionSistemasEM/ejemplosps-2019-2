#include <stdio.h>
#include <unistd.h>
 #include <sys/types.h>


int main(){

	pid_t pid = fork();

	int a = 4;
	if(pid == 0){
		a++;
		printf("proceso hijo pid = %d, ppid = %d\n", getpid(), getppid());
		execl("./programa2", "./programa2", "-n", "48", (void *)0);
		printf("Exec fallo\n");
	}
	else{
		a--;
		printf("Proceso padre pid = %d\n", getpid());
	}
	printf("a = %d\n", a);
	return 0;
	

}
