#include <stdio.h>
#include <unistd.h>
 #include <sys/types.h>


int main(){
	int a = 4;
	pid_t pid = fork();

	if(pid == 0){
		a++;
		printf("2");
		pid = fork();
		if(pid != 0){
			printf("4");
			wait(NULL);
		}
		print("8");
	}
	else{
		printf("1")
		a--;
		wait(NULL);
	}
	printf("a = %d\n", a);
	return 0;
	

}
