#include <stdio.h>
#include <unistd.h>
 #include <sys/types.h>


int main(){
	int a = 4;
	pid_t pid = fork();

	if(pid == 0){
		a++;
		printf("proceso hijo pid = %d, ppid = %d\n", getpid(), getppid());
	}
	else{
		a--;
		printf("Proceso padre pid = %d\n", getpid());
	}
	printf("a = %d\n", a);
	return 0;
	

}
