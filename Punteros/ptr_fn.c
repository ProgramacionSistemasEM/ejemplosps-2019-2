#include <stdio.h>


typedef int (*fn)(int, float);

int una_funcion(int a, float b){
	printf("%d %.2f\n", a, b);
	return 0;
}


int main(){
	fn ptr_a_funcion;
	ptr_a_funcion = una_funcion;

	ptr_a_funcion(4, 5.5f);
	return 0;
}
