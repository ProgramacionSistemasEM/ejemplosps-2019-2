#include <stdio.h>
#include <stdlib.h>

int fn(long **p){
	*p = malloc(5000);
	return 0;
}


int main(){
	long *q;
	int resultado = fn(&q);
	printf("Resultado fn %d\n",resultado);
	q[100] = 6666;
	printf("q[100] = %ld\n", q[100]);
	return 0;
}
